package obrazy;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import javax.imageio.ImageIO;

public class Lab06 {
    public static void run(BufferedImage in1, BufferedImage in2, BufferedImage in3, BufferedImage in4, BufferedImage in5) {
        try {
            BufferedImage out = Lab06.cien_maska(in1);
            ImageIO.write(out, "png", new File("12-t1-M.png"));
            System.out.println("Maska pierwszego gotowa!");
            out = Lab06.cien_maska(in2);
            ImageIO.write(out, "png", new File("12-t2-M.png"));
            System.out.println("Maska drugiego gotowa!");
            out = Lab06.cien_maska(in3);
            ImageIO.write(out, "png", new File("12-t3-M.png"));
            System.out.println("Maska trzeciego gotowa!");
            out = Lab06.cien_maska(in4);
            ImageIO.write(out, "png", new File("12-t4-M.png"));
            System.out.println("Maska czwartego gotowa!");
            out = Lab06.cien_maska(in5);
            ImageIO.write(out, "png", new File("12-t5-M.png"));
            System.out.println("Maska piatego gotowa!");

            out = Lab06.cien_KMM(in1);
            ImageIO.write(out, "png", new File("12-t1-KMM.png"));
            System.out.println("KMM pierwszego gotowe!");
            out = Lab06.cien_KMM(in2);
            ImageIO.write(out, "png", new File("12-t2-KMM.png"));
            System.out.println("KMM drugiego gotowe!");
            out = Lab06.cien_KMM(in3);
            ImageIO.write(out, "png", new File("12-t3-KMM.png"));
            System.out.println("KMM trzeciego gotowe!");
            out = Lab06.cien_KMM(in4);
            ImageIO.write(out, "png", new File("12-t4-KMM.png"));
            System.out.println("KMM czwartego gotowe!");
            out = Lab06.cien_KMM(in5);
            ImageIO.write(out, "png", new File("12-t5-KMM.png"));
            System.out.println("KMM piatego gotowe!");

        } catch (IOException e) {
            System.out.println("W module Lab06 padło: " + e.toString());
        }
    }

    //poszczególne wykorzystywane kolory, czerwonym oznaczane są elementy maski, które są nieistotne
    private static final int czarny = Color.BLACK.getRGB();
    private static final int bialy = Color.WHITE.getRGB();
    private static final int czerwony = Color.RED.getRGB();

    private static int[][][] maski; // uzupełnij maski

    // funkcja porównująca otoczenie piksela z maską
    private static int porownaj(BufferedImage biezacy, int i, int j) {
        int zlicz;
        for (int m = 0; m < maski.length; m++) {
            zlicz = 0;
            // Tu uzupełnij kod porównania maski
            if (true) return bialy; // Tu zmień warunek
        }
        return czarny;
    }

    // Ścienianie przez maskę
    public static BufferedImage cien_maska(BufferedImage in) {

        int szerokosc = in.getWidth() + 2;
        int wysokosc = in.getHeight() + 2;

        BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), in.getType());

        BufferedImage out1 = RGB.powiekszBialymi(in, 1);
        BufferedImage out2 = RGB.powiekszBialymi(in, 1);

        //***************************************
        boolean zmiany = true;


        while (zmiany) {
            zmiany = false;
            for (int i = 1; i < szerokosc - 1; i++) {
                for (int j = 1; j < wysokosc - 1; j++) {
                    if (RGB.getR(out1.getRGB(i, j)) == 0) {

                        if (spelniaMaske0(out1, i, j) ||
                                spelniaMaske90(out1, i, j) ||
                                spelniaMaske180(out1, i, j) ||
                                spelniaMaske270(out1, i, j)) {
                            out2.setRGB(i, j, RGB.toRGB(255, 255, 255));
                        } else {
                            out2.setRGB(i, j, RGB.toRGB(0, 0, 0));
                        }
                        if (out2.getRGB(i, j) != out1.getRGB(i, j)) {
                            zmiany = true;
                        }

                    }
                }
            }
            for (int i = 0; i < out1.getWidth(); i++) {
                for (int j = 0; j < out1.getHeight(); j++) {
                    out1.setRGB(i, j, out2.getRGB(i, j));
                }
            }
        }
        //****************************************/
        for (int i = 1; i < szerokosc - 1; i++) {
            for (int j = 1; j < wysokosc - 1; j++) {
                out.setRGB(i - 1, j - 1, out1.getRGB(i, j));
            }
        }
        return out;
    }

    private static boolean spelniaMaske270(BufferedImage out1, int i, int j) {
        int maska[][] = {
                {-1, -1, 1},
                {0, 1, 1},
                {-1, -1, 1}
        };
        return spelniaMaske(maska, out1, i, j);
    }

    private static boolean spelniaMaske180(BufferedImage out1, int i, int j) {
        int maska[][] = {
                {1, 1, 1},
                {-1, 1, -1},
                {-1, 0, -1}
        };

        return spelniaMaske(maska, out1, i, j);
    }

    private static boolean spelniaMaske90(BufferedImage out1, int i, int j) {
        int maska[][] = {
                {1, -1, -1},
                {1, 1, 0},
                {1, -1, -1}
        };

        return spelniaMaske(maska, out1, i, j);
    }

    private static boolean spelniaMaske0(BufferedImage out1, int i, int j) {
        int maska[][] = {
                {-1, 0, -1},
                {-1, 1, -1},
                {1, 1, 1}
        };

        return spelniaMaske(maska, out1, i, j);
    }

    private static boolean spelniaMaske(int[][] maska, BufferedImage out1, int i, int j) {
        for (int x = i - 1; x <= i + 1; x++) {
            for (int y = j - 1; y <= j + 1; y++) {
                if (maska[x - (i - 1)][y - (j - 1)] == -1) {
                    continue;
                }
                if (maska[x - (i - 1)][y - (j - 1)] == 0) {
                    if (RGB.getR(out1.getRGB(x, y)) == 255) {
                        continue;
                    } else {
                        return false;
                    }
                }
                if (maska[x - (i - 1)][y - (j - 1)] == 1) {
                    if (RGB.getR(out1.getRGB(x, y)) == 0) {
                        continue;
                    } else {
                        return false;
                    }
                }
            }
        }
        return true;
    }


    // wartości masek do KMM
    private final static int[] wyciecia = {3, 5, 7, 12, 13, 14, 15, 20,
            21, 22, 23, 28, 29, 30, 31, 48,
            52, 53, 54, 55, 56, 60, 61, 62,
            63, 65, 67, 69, 71, 77, 79, 80,
            81, 83, 84, 85, 86, 87, 88, 89,
            91, 92, 93, 94, 95, 97, 99, 101,
            103, 109, 111, 112, 113, 115, 116, 117,
            118, 119, 120, 121, 123, 124, 125, 126,
            127, 131, 133, 135, 141, 143, 149, 151,
            157, 159, 181, 183, 189, 191, 192, 193,
            195, 197, 199, 205, 207, 208, 209, 211,
            212, 213, 214, 215, 216, 217, 219, 220,
            221, 222, 223, 224, 225, 227, 229, 231,
            237, 239, 240, 241, 243, 244, 245, 246,
            247, 248, 249, 251, 252, 253, 254, 255};

    private final static int[] czworki = {3, 6, 7, 12, 14, 15, 24, 28,
            30, 48, 56, 60, 96, 112, 120, 129,
            131, 135, 192, 193, 195, 224, 225, 240};

    private static int[][] maska = {{128, 64, 32}, {1, 0, 16}, {2, 4, 8}};

    // Ścienianie przez KMM
    public static BufferedImage cien_KMM(BufferedImage in) {
        int[][] biezacy = new int[in.getWidth() + 2][in.getHeight() + 2];

        BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), in.getType());

        int szerokosc = in.getWidth() + 2;
        int wysokosc = in.getHeight() + 2;

        for (int i = 0; i < szerokosc; i++) {
            for (int j = 0; j < wysokosc; j++) {
                // tu stwórz tablicę wypełnioną zerami i jedynkami
                if (i == 0 || j == 0 || i == szerokosc - 1 || j == wysokosc - 1) {
                    biezacy[i][j] = 0;
                    continue;
                }
                int inI = i - 1;
                int inJ = j - 1;
                if (RGB.getR(in.getRGB(inI, inJ)) == 255) {
                    biezacy[i][j] = 0;
                } else {
                    biezacy[i][j] = 1;
                }
            }
        }

        int[][] oldBiezacy = new int[biezacy.length][biezacy[0].length];

        do {
            skopiujTablice(biezacy, oldBiezacy);
            for (int i = 1; i < szerokosc - 1; i++) {
                for (int j = 1; j < wysokosc - 1; j++) {
                    if (biezacy[i][j] == 1) {
                        if (biezacy[i - 1][j] == 0 || biezacy[i + 1][j] == 0 || biezacy[i][j - 1] == 0 || biezacy[i][j + 1] == 0) {
                            biezacy[i][j] = 2;
                        }
                    }
                }
            }

            for (int i = 1; i < szerokosc - 1; i++) {
                for (int j = 1; j < wysokosc - 1; j++) {
                    if (biezacy[i][j] == 1) {
                        if (biezacy[i - 1][j - 1] == 0 || biezacy[i + 1][j + 1] == 0 || biezacy[i + 1][j - 1] == 0 || biezacy[i - 1][j + 1] == 0) {
                            biezacy[i][j] = 3;
                        }
                    }
                }
            }

            for (int i = 1; i < szerokosc - 1; i++) {
                for (int j = 1; j < wysokosc - 1; j++) {
                    if (biezacy[i][j] == 2) {
                        if (wartoscZawieraSieW(czworki, obliczWage(biezacy, i, j))) {
                            biezacy[i][j] = 4;
                        }
                    }
                }
            }

            for (int i = 1; i < szerokosc - 1; i++) {
                for (int j = 1; j < wysokosc - 1; j++) {
                    if (biezacy[i][j] == 4) {
                        if (wartoscZawieraSieW(wyciecia, obliczWage(biezacy, i, j))) {
                            biezacy[i][j] = 0;
                        } else {
                            biezacy[i][j] = 1;
                        }
                    }
                }
            }

            for (int i = 1; i < szerokosc - 1; i++) {
                for (int j = 1; j < wysokosc - 1; j++) {
                    if (biezacy[i][j] == 2) {
                        if (wartoscZawieraSieW(wyciecia, obliczWage(biezacy, i, j))) {
                            biezacy[i][j] = 0;
                        } else {
                            biezacy[i][j] = 1;
                        }
                    }
                }
            }

            for (int i = 1; i < szerokosc - 1; i++) {
                for (int j = 1; j < wysokosc - 1; j++) {
                    if (biezacy[i][j] == 3) {
                        if (wartoscZawieraSieW(wyciecia, obliczWage(biezacy, i, j))) {
                            biezacy[i][j] = 0;
                        } else {
                            biezacy[i][j] = 1;
                        }
                    }
                }
            }

        } while (rozniSie(oldBiezacy, biezacy));

        for (int i = 0; i < out.getWidth(); i++) {
            for (int j = 0; j < out.getHeight(); j++) {
                if (biezacy[i + 1][j + 1] == 0) {
                    out.setRGB(i, j, RGB.toRGB(255, 255, 255));
                }
                if (biezacy[i + 1][j + 1] == 1) {
                    out.setRGB(i, j, RGB.toRGB(0, 0, 0));
                }
            }
        }

        return out;
    }

    private static boolean wartoscZawieraSieW(int[] wyciecia, int waga) {
        for(int el : wyciecia){
            if(el == waga){
                return true;
            }
        }
        return false;
    }

    private static int obliczWage(int[][] biezacy, int i, int j) {
        int[][] interesujacePole = new int[][]{
                {biezacy[i - 1][j - 1], biezacy[i - 1][j], biezacy[i - 1][j + 1]},
                {biezacy[i][j - 1], biezacy[i][j], biezacy[i][j + 1]},
                {biezacy[i + 1][j - 1], biezacy[i + 1][j], biezacy[i + 1][j + 1]}
        };

        int waga = 0;
        for (int ii = 0; ii < 3; ii++) {
            for (int jj = 0; jj < 3; jj++) {
                if (interesujacePole[ii][jj] != 0) {
                    waga += maska[ii][jj];
                }
            }
        }
        return waga;
    }

    private static boolean rozniSie(int[][] oldBiezacy, int[][] biezacy) {
        for (int i = 0; i < biezacy.length; i++) {
            for (int j = 0; j < biezacy[0].length; j++) {
                if (biezacy[i][j] != oldBiezacy[i][j]) {
                    return true;
                }
            }
        }
        return false;
    }

    private static void skopiujTablice(int[][] source, int[][] target) {
        for (int i = 0; i < source.length; i++) {
            for (int j = 0; j < source[0].length; j++) {
                target[i][j] = source[i][j];
            }
        }
    }

}
