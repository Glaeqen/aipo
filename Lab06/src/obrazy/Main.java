/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package obrazy;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author Adam
 */
public class Main {

    public static void main(String[] args) {
        try {

            BufferedImage in1 = ImageIO.read(new File("12-t1.png"));
            BufferedImage in2 = ImageIO.read(new File("12-t2.png"));
            BufferedImage in3 = ImageIO.read(new File("12-t3.png"));
            BufferedImage in4 = ImageIO.read(new File("12-t4.png"));
            BufferedImage in5 = ImageIO.read(new File("12-t5.png"));

            Lab06.run(in1,in2,in3,in4,in5);

        } catch (IOException e) {
            System.out.println("Padło: " + e.toString());
        }

    }
}
