#!/usr/bin/bash
pdflatex -interaction nonstopmode -shell-escape -halt-on-error -file-line-error 0*.tex
while inotifywait -q -q -e close_write,moved_to *.tex;
do
	  clear
	  pdflatex -interaction nonstopmode -shell-escape -halt-on-error -file-line-error 0*.tex
	  notify-send $?
done
