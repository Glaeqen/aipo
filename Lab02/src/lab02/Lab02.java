package lab02;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Lab02 {
    public static void run(BufferedImage in) {
        try {
            BufferedImage out = Lab02.skalowanie(in, (float) (0.4));
            out = Lab02.skalowanie(out, (float) (2.5));
            ImageIO.write(out, "jpg", new File("05.jpg"));
            out = Lab02.progowanieSG(in);
            ImageIO.write(out, "jpg", new File("06a.jpg"));

            int size[] = {2, 5, 7, 10, 12};
            for (int pom_licz = 0; pom_licz < size.length; pom_licz++) {
                out = Lab02.progowanieL(in, size[pom_licz]);
                ImageIO.write(out, "jpg", new File("06b-" + (2 * size[pom_licz] + 1) + ".jpg"));
            }

            int size2[] = {15, 25, 35};
            for (int pom_licz = 0; pom_licz < size2.length; pom_licz++) {
                out = Lab02.progowanieLSG(in, 15, size2[pom_licz]);
                ImageIO.write(out, "jpg", new File("06c-" + size2[pom_licz] + ".jpg"));
            }

            int size3[][][] = {{{1, 1, 1}, {1, 1, 1}, {1, 1, 1}},
                    {{1, 1, 1}, {1, 2, 1}, {1, 1, 1}},
                    {{1, 2, 1}, {2, 4, 2}, {1, 2, 1}},
                    {{0, -1, 0}, {-1, 5, -1}, {0, -1, 0}},
                    {{-1, -1, -1}, {-1, 9, -1}, {-1, -1, -1}},
                    {{1, -2, 1}, {-2, 5, -2}, {1, -2, 1}}
            };
            for (int pom_licz = 0; pom_licz < size3.length; pom_licz++) {
                out = Lab02.filtrSplotowy(in, size3[pom_licz]);
                ImageIO.write(out, "jpg", new File("07-" + pom_licz + ".jpg"));
            }


        } catch (IOException e) {
            System.out.println("W module Lab02 padło: " + e.toString());
        }
    }

    // skalowanie obrazu
    public static BufferedImage skalowanie(BufferedImage in, float skala) {
        BufferedImage out = new BufferedImage(Math.round(skala * in.getWidth()), Math.round(skala * in.getHeight()), in.getType());
        int r, g, b, height, width;
        width = out.getWidth();
        height = out.getHeight();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int oldI = (int) (i / skala);
                int oldJ = (int) (j / skala);
                out.setRGB(i, j, in.getRGB(oldI, oldJ));
            }
        }
        return out;
    }

    // progowanie po średniej globalnej
    public static BufferedImage progowanieSG(BufferedImage in) {
        BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), in.getType());
        BufferedImage szary = szarosciN(in); //Tu potrzebujesz kodu z pierwszych zajęć
        int r, g, b, height, width;
        width = out.getWidth();
        height = out.getHeight();
        // obliczanie średniej
        int srednia = 0;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int pixel = szary.getRGB(i, j);
                srednia += RGB.getR(pixel);
            }
        }

        srednia = (int) (srednia / (1.0 * width * height));
        // Tu wypełnij właściwym kodem

        // właściwe progowanie
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int pixel = szary.getRGB(i, j);
                r = RGB.getR(pixel);
                if (r > srednia) {
                    r = 0xff;
                } else {
                    r = 0x00;
                }
                out.setRGB(i, j, RGB.toRGB(r, r, r));
            }
        }

        return out;
    }

    // tworzenie odcieni szarości - 0.3*R+0.59*G+0.11*B
    public static BufferedImage szarosciN(BufferedImage in) {
        BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), in.getType());
        int width, height;
        width = out.getWidth();
        height = out.getHeight();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int currentRGB = in.getRGB(i, j);
                int r = RGB.getR(currentRGB);
                int g = RGB.getG(currentRGB);
                int b = RGB.getB(currentRGB);
                int pixel = (int) (0.3 * r + 0.59 * g + 0.11 * b);
                out.setRGB(i, j, RGB.toRGB(pixel, pixel, pixel));
            }
        }
        return out;
    }

    // progowanie lokalne
    public static BufferedImage progowanieL(BufferedImage in, int rozmiar) {
        BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), in.getType());
        BufferedImage szary = szarosciN(in); //Tu potrzebujesz kodu z pierwszych zajęć

        rozmiar /= 2;

        int height, width;
        int srednia_lokalna;
        width = out.getWidth();
        height = out.getHeight();
        int r = 0;
        int count;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                r = RGB.getR(szary.getRGB(i, j));
                srednia_lokalna = 0;
                count = 0;
                for (int x = i - rozmiar; x <= i + rozmiar; x++) {
                    for (int y = j - rozmiar; y <= j + rozmiar; y++) {
                        if (x >= 0 && x < width && y >= 0 && y < height) {
                            srednia_lokalna += RGB.getR(szary.getRGB(x, y));
                            count++;
                        }
                    }
                }
                srednia_lokalna = srednia_lokalna / count;
                if (r >= srednia_lokalna) {
                    out.setRGB(i, j, RGB.toRGB(0xff, 0xff, 0xff));
                } else {
                    out.setRGB(i, j, RGB.toRGB(0, 0, 0));
                }
            }
        }

        return out;
    }

    // progowanie lokalne ze średnią globalną
    public static BufferedImage progowanieLSG(BufferedImage in, int rozmiar, int odbieganie) {
        BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), in.getType());
        BufferedImage szary = szarosciN(in); //Tu potrzebujesz kodu z pierwszych zajęć
        int srednia_lokalna;
        int height, width;
        width = out.getWidth();
        height = out.getHeight();
        // obliczanie średniej
        int srednia = 0;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int pixel = szary.getRGB(i, j);
                srednia += RGB.getR(pixel);
            }
        }

        srednia = (int) (srednia / (1.0 * width * height));
        rozmiar /= 2;

        int r;
        int count;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                r = RGB.getR(szary.getRGB(i, j));
                srednia_lokalna = 0;
                count = 0;
                for (int x = i - rozmiar; x <= i + rozmiar; x++) {
                    for (int y = j - rozmiar; y <= j + rozmiar; y++) {
                        if (x >= 0 && x < width && y >= 0 && y < height) {
                            srednia_lokalna += RGB.getR(szary.getRGB(x, y));
                            count++;
                        }
                    }
                }
                srednia_lokalna = srednia_lokalna / count;
                if (Math.abs(srednia - srednia_lokalna) > odbieganie) {
                    if (r > srednia) {
                        out.setRGB(i, j, RGB.toRGB(0xff, 0xff, 0xff));
                    } else {
                        out.setRGB(i, j, RGB.toRGB(0x00, 0x00, 0x00));
                    }
                } else {
                    out.setRGB(i, j, RGB.toRGB(srednia_lokalna, srednia_lokalna, srednia_lokalna));
                    if (r >= srednia_lokalna) {
                        out.setRGB(i, j, RGB.toRGB(0xff, 0xff, 0xff));
                    } else {
                        out.setRGB(i, j, RGB.toRGB(0x00, 0x00, 0x00));
                    }
                }
            }
        }

        return out;
    }

    // filtry splotowe
    public static BufferedImage filtrSplotowy(BufferedImage in, int maska[][]) {
        BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), in.getType());
        int height, width, mheight, mwidth;
        int r, g, b;
        mheight = maska[0].length;
        mwidth = maska.length;
        int mwidthhalf = (int) (maska.length / 2);
        int mheighthalf = (int) (maska[0].length / 2);
        width = out.getWidth() - mwidthhalf;
        height = out.getHeight() - mheighthalf;

        int ilosc = 0;
        for (int i = 0; i < mwidth; i++) {
            for (int j = 0; j < mheight; j++) {
                ilosc += maska[i][j];
            }
        }

        // Tu wypełnij właściwym kodem

        for (int i = mwidthhalf; i < width; i++) {
            for (int j = mheighthalf; j < height; j++) {
                int newValueR= 0;
                int newValueG= 0;
                int newValueB= 0;
                int kOffset = i - mwidthhalf;
                int lOffset = j - mheighthalf;
                for(int k = kOffset; k <= i + mwidthhalf; k++){
                    for(int l = lOffset; l <= j + mheighthalf; l++){
                        int pixel = in.getRGB(k, l);
                        newValueR += maska[k-kOffset][l-lOffset]*RGB.getR(pixel);
                        newValueG += maska[k-kOffset][l-lOffset]*RGB.getG(pixel);
                        newValueB += maska[k-kOffset][l-lOffset]*RGB.getB(pixel);
                    }
                }
                r = newValueR/ilosc;
                g = newValueG/ilosc;
                b = newValueB/ilosc;
                if(r > 255){
                    r = 255;
                }
                if(r < 0){
                    r = 0;
                }
                if(g > 255){
                    g = 255;
                }
                if(g < 0){
                    g = 0;
                }
                if(b > 255){
                    b = 255;
                }
                if(b < 0){
                    b = 0;
                }
                out.setRGB(i, j, RGB.toRGB(r, g, b));
            }
        }

        return out;
    }
}
