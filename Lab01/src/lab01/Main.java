/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lab01;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author Adam
 */
public class Main {

    public static void main(String[] args) {
        try {
            BufferedImage in = ImageIO.read(new File("../1.jpg"));
            Lab01.run(in);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
