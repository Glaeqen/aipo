package lab01;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Lab01 {

    public static void run(BufferedImage in) {
        try {
            BufferedImage out = Lab01.negatyw(in);
            ImageIO.write(out, "jpg", new File("02.jpg"));
            out = Lab01.histnorm(in);
            ImageIO.write(out, "jpg", new File("01b.jpg"));
            out = Lab01.szarosciS(in);
            ImageIO.write(out, "jpg", new File("03.jpg"));
            out = Lab01.histnorm(out);
            ImageIO.write(out, "jpg", new File("03b.jpg"));
            out = Lab01.szarosciN(in);
            ImageIO.write(out, "jpg", new File("04.jpg"));
            out = Lab01.histnorm(out);
            ImageIO.write(out, "jpg", new File("04b.jpg"));
        } catch (IOException e) {
            System.out.println("W module Lab01 padło: " + e.toString());
        }
    }

    // tworzenie negatywu - spacer po wszystkich pikselach i zamiana ich wartości R, G i B na 255 - wartość
    public static BufferedImage negatyw(BufferedImage in) {
        BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), in.getType());
        int height, width;
        width = out.getWidth();
        height = out.getHeight();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int currentRGB = in.getRGB(i, j);
                int newR = 0xff - RGB.getR(currentRGB);
                int newG = 0xff - RGB.getG(currentRGB);
                int newB = 0xff - RGB.getB(currentRGB);
                out.setRGB(i, j, RGB.toRGB(newR, newG, newB));
            }
        }
        return out;
    }

    // tworzenie odcieni szarości - (R+G+B)/3
    public static BufferedImage szarosciS(BufferedImage in) {
        BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), in.getType());
        int width, height;
        width = out.getWidth();
        height = out.getHeight();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int currentRGB = in.getRGB(i, j);
                int r = RGB.getR(currentRGB);
                int g = RGB.getG(currentRGB);
                int b = RGB.getB(currentRGB);
                int pixel = (r + g + b) / 3;
                out.setRGB(i, j, RGB.toRGB(pixel, pixel, pixel));
            }
        }
        return out;
    }

    // tworzenie odcieni szarości - 0.3*R+0.59*G+0.11*B
    public static BufferedImage szarosciN(BufferedImage in) {
        BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), in.getType());
        int width, height;
        width = out.getWidth();
        height = out.getHeight();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int currentRGB = in.getRGB(i, j);
                int r = RGB.getR(currentRGB);
                int g = RGB.getG(currentRGB);
                int b = RGB.getB(currentRGB);
                int pixel = (int)(0.3*r + 0.59*g + 0.11*b);
                out.setRGB(i, j, RGB.toRGB(pixel, pixel, pixel));
            }
        }
        return out;
    }

    // normalizacja histogramu
    public static BufferedImage histnorm(BufferedImage in) {
        BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), in.getType());
        int width, height, R, G, B;
        width = out.getWidth();
        height = out.getHeight();
        // szukanie minimów i maksimów
        int minR = 0xff;
        int minG = 0xff;
        int minB = 0xff;
        int maxR = 0x00;
        int maxG = 0x00;
        int maxB = 0x00;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int currentRGB = in.getRGB(i, j);
                int r = RGB.getR(currentRGB);
                int g = RGB.getG(currentRGB);
                int b = RGB.getB(currentRGB);
                if(r > maxR){
                    maxR = r;
                }
                if(g > maxG){
                    maxG = g;
                }
                if(b > maxB){
                    maxB = b;
                }
                if(r < minR){
                    minR = r;
                }
                if(g < minG){
                    minG = g;
                }
                if(b < minB){
                    minB = b;
                }
            }
        }
        // właściwa normalizacja

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int currentRGB = in.getRGB(i, j);
                int r = RGB.getR(currentRGB);
                int g = RGB.getG(currentRGB);
                int b = RGB.getB(currentRGB);
                int newR = (int)(255*(r-minR)/(1.0*maxR-minR));
                int newG = (int)(255*(g-minG)/(1.0*maxG-minG));
                int newB = (int)(255*(b-minB)/(1.0*maxB-minB));
                out.setRGB(i,j, RGB.toRGB(newR, newG, newB));
            }
        }
        return out;
    }
}
