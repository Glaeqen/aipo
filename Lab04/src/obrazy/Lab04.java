package obrazy;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import javax.imageio.ImageIO;

public class Lab04 {
    public static void run(BufferedImage in) {
        try {
            BufferedImage out, out2;
            int tab[] = {5, 10, 15};
            for (int i = 0; i < tab.length; i++) {
                out = Lab04.solIpieprz(in, tab[i]);
                ImageIO.write(out, "jpg", new File("11-sp" + tab[i] + ".jpg"));
                System.out.println("Szum sól i pieprz z prawdopodobieństwem " + tab[i] + "% gotowy!");
                for (int j = 1; j < 4; j++) {
                    out2 = Lab04.odszumianieS(out, j);
                    ImageIO.write(out2, "jpg", new File("11-sp" + tab[i] + "-oS-" + (2 * j + 1) + ".jpg"));
                    System.out.println("Odszumianie przez średnią obrazu sól i pieprz z prawdopodobieństwem " + tab[i] + "% z otoczeniem " + (2 * j + 1) + " gotowe!");
                    out2 = Lab04.odszumianieM(out, j);
                    ImageIO.write(out2, "jpg", new File("11-sp" + tab[i] + "-oM-" + (2 * j + 1) + ".jpg"));
                    System.out.println("Odszumianie przez medianę obrazu sól i pieprz z prawdopodobieństwem " + tab[i] + "% z otoczeniem " + (2 * j + 1) + " gotowe!");
                    out2 = Lab04.odszumianieMU(out, j);
                    ImageIO.write(out2, "jpg", new File("11-sp" + tab[i] + "-oUM-" + (2 * j + 1) + ".jpg"));
                    System.out.println("Odszumianie przez ulepszoną medianę obrazu sól i pieprz z prawdopodobieństwem " + tab[i] + "% z otoczeniem " + (2 * j + 1) + " gotowe!");
                }
            }

            for (int i = 0; i < tab.length; i++) {
                out = Lab04.rownomiernyT(in, tab[i]);
                ImageIO.write(out, "jpg", new File("11-srT" + tab[i] + ".jpg"));
                System.out.println("Szum równomierny zgodny na każdym kanale z prawdopodobieństwem " + tab[i] + "% gotowy!");
            }
            for (int i = 0; i < tab.length; i++) {
                out = Lab04.rownomiernyN(in, tab[i]);
                ImageIO.write(out, "jpg", new File("11-srN" + tab[i] + ".jpg"));
                System.out.println("Szum równomierny z różnymi wartościami na kanałach z prawdopodobieństwem " + tab[i] + "% gotowy!");
                for (int j = 1; j < 4; j++) {
                    out2 = Lab04.odszumianieS(out, j);
                    ImageIO.write(out2, "jpg", new File("11-srN" + tab[i] + "-oS-" + (2 * j + 1) + ".jpg"));
                    System.out.println("Odszumianie przez średnią obrazu z szumem równomiernym z różnymi wartościami na kanałach z prawdopodobieństwem " + tab[i] + "% z otoczeniem " + (2 * j + 1) + " gotowe!");
                    out2 = Lab04.odszumianieM(out, j);
                    ImageIO.write(out2, "jpg", new File("11-srN" + tab[i] + "-oM-" + (2 * j + 1) + ".jpg"));
                    System.out.println("Odszumianie przez medianę obrazu z szumem równomiernym z różnymi wartościami na kanałach z prawdopodobieństwem " + tab[i] + "% z otoczeniem " + (2 * j + 1) + " gotowe!");
                    out2 = Lab04.odszumianieMU(out, j);
                    ImageIO.write(out2, "jpg", new File("11-srN" + tab[i] + "-oUM-" + (2 * j + 1) + ".jpg"));
                    System.out.println("Odszumianie przez ulepszoną medianę obrazu z szumem równomiernym z różnymi wartościami na kanałach z prawdopodobieństwem " + tab[i] + "% z otoczeniem " + (2 * j + 1) + " gotowe!");
                }
            }
        } catch (IOException e) {
            System.out.println("W module Lab01 padło: " + e.toString());
        }
    }

    // zaszumianie sól i pieprz
    public static BufferedImage solIpieprz(BufferedImage in, int procent) {
        procent = 100 - procent;
        int height, width;
        BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), in.getType());
        width = out.getWidth();
        height = out.getHeight();
        Random generator = new Random();
        Random generator2 = new Random();
        int czarny = 0x00000000;
        int bialy = 0x00ffffff;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (generator.nextDouble() >= ((1.0 * procent) / 100.0)) {
                    if (generator2.nextDouble() < 0.5) {
                        out.setRGB(i, j, czarny);
                    } else {
                        out.setRGB(i, j, bialy);
                    }
                } else {
                    out.setRGB(i, j, in.getRGB(i, j));
                }
            }
        }
        return out;
    }

    public static int assureRange(int channel) {
        if (channel > 255) {
            return 255;
        }
        if (channel < 0) {
            return 0;
        }
        return channel;
    }

    // zaszumianie równomierne, każdy kanał ten sam
    public static BufferedImage rownomiernyT(BufferedImage in, int procent) {
        procent = 100 - procent;
        int height, width;
        BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), in.getType());
        width = out.getWidth();
        height = out.getHeight();
        Random generator = new Random();
        Random generator2 = new Random();
        Random generator3 = new Random();

        int war, r, g, b;
        double rand;

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (generator.nextDouble() >= ((1.0 * procent) / 100.0)) {
                    rand = generator.nextDouble() * 9 + 21;
                    if (generator.nextDouble() > 0.5) {
                        rand *= -1;
                    }
                    war = in.getRGB(i, j);
                    r = RGB.getR(war);
                    g = RGB.getG(war);
                    b = RGB.getB(war);
                    r += rand;
                    g += rand;
                    b += rand;
                    r = assureRange(r);
                    g = assureRange(g);
                    b = assureRange(b);
                    out.setRGB(i, j, RGB.toRGB(r, g, b));
                } else {
                    out.setRGB(i, j, in.getRGB(i, j));
                }
            }
        }
        return out;
    }

    // zaszumianie równomierne, każdy kanał inny
    public static BufferedImage rownomiernyN(BufferedImage in, int procent) {
        procent = 100 - procent;
        int height, width;
        BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), in.getType());
        width = out.getWidth();
        height = out.getHeight();
        Random generator = new Random();
        Random generator2 = new Random();
        Random generator3 = new Random();

        int war, r, g, b;
        double randr, randg, randb;

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (generator.nextDouble() >= ((1.0 * procent) / 100.0)) {
                    randr = generator.nextDouble() * 9 + 21;
                    randg = generator.nextDouble() * 9 + 21;
                    randb = generator.nextDouble() * 9 + 21;
                    if (generator.nextDouble() > 0.5) {
                        randr *= -1;
                    }
                    if (generator.nextDouble() > 0.5) {
                        randg *= -1;
                    }
                    if (generator.nextDouble() > 0.5) {
                        randb *= -1;
                    }
                    war = in.getRGB(i, j);
                    r = RGB.getR(war);
                    g = RGB.getG(war);
                    b = RGB.getB(war);
                    r += randr;
                    g += randg;
                    b += randb;
                    r = assureRange(r);
                    g = assureRange(g);
                    b = assureRange(b);
                    out.setRGB(i, j, RGB.toRGB(r, g, b));
                } else {
                    out.setRGB(i, j, in.getRGB(i, j));
                }
            }
        }
        return out;
    }

    // odszumianie przez średnią
    public static BufferedImage odszumianieS(BufferedImage in, int rozmiar) {
        BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), in.getType());
        int height, width;
        int SLR, SLG, SLB;
        int sumcia;
        width = out.getWidth();
        height = out.getHeight();

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                sumcia = 0;
                SLR = 0;
                SLG = 0;
                SLB = 0;
                for (int k = i - rozmiar; k < i + rozmiar + 1; k++) {
                    for (int l = j - rozmiar; l < j + rozmiar + 1; l++) {
                        if (k < 0 || k >= width || l < 0 || l >= height) {
                            continue;
                        }
                        int val = in.getRGB(k, l);
                        SLR += RGB.getR(val);
                        SLG += RGB.getG(val);
                        SLB += RGB.getB(val);
                        sumcia++;
                    }
                }
                out.setRGB(i, j, RGB.toRGB(SLR / sumcia, SLG / sumcia, SLB / sumcia));
            }
        }

        return out;
    }

    // odszumianie przez medianę
    public static BufferedImage odszumianieM(BufferedImage in, int rozmiar) {
        BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), in.getType());
        int height, width;
        int sumcia;
        width = out.getWidth();
        height = out.getHeight();
        List<Integer> r = new ArrayList<>();
        List<Integer> g = new ArrayList<>();
        List<Integer> b = new ArrayList<>();
        int srodek;

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                r = new ArrayList<>();
                g = new ArrayList<>();
                b = new ArrayList<>();
                for (int k = i - rozmiar; k < i + rozmiar + 1; k++) {
                    for (int l = j - rozmiar; l < j + rozmiar + 1; l++) {
                        if (k < 0 || k >= width || l < 0 || l >= height) {
                            continue;
                        }
                        int val = in.getRGB(k, l);
                        r.add(RGB.getR(val));
                        g.add(RGB.getG(val));
                        b.add(RGB.getB(val));
                    }
                }
                Collections.sort(r);
                Collections.sort(g);
                Collections.sort(b);
                out.setRGB(i, j, RGB.toRGB(r.get(r.size() / 2),
                        g.get(g.size() / 2),
                        b.get(b.size() / 2)));
            }
        }

        return out;
    }

    // odszumianie przez ulepszoną medianę
    public static BufferedImage odszumianieMU(BufferedImage in, int rozmiar) {
        BufferedImage out = new BufferedImage(in.getWidth(), in.getHeight(), in.getType());
        int height, width;
        boolean jest;
        int sumcia;
        width = out.getWidth();
        height = out.getHeight();
        List<Integer> r, g, b;

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                r = new ArrayList<>();
                g = new ArrayList<>();
                b = new ArrayList<>();
                for (int k = i - rozmiar; k < i + rozmiar + 1; k++) {
                    for (int l = j - rozmiar; l < j + rozmiar + 1; l++) {
                        if (k < 0 || k >= width || l < 0 || l >= height) {
                            continue;
                        }
                        int val = in.getRGB(k, l);
                        r.add(RGB.getR(val));
                        g.add(RGB.getG(val));
                        b.add(RGB.getB(val));
                    }
                }
                Collections.sort(r);
                Collections.sort(g);
                Collections.sort(b);
                int oldVal = in.getRGB(i, j);
                int newR = FineMedian(r, r.get(r.size() / 2 -1), RGB.getR(oldVal));
                int newG = FineMedian(g, g.get(g.size() / 2 -1), RGB.getG(oldVal));
                int newB = FineMedian(b, b.get(b.size() / 2 -1), RGB.getB(oldVal));

                out.setRGB(i, j, RGB.toRGB(newR, newG, newB));
            }
        }

        return out;
    }

    private static int FineMedian(List<Integer> vals, int median, int oldVal) {
        for (int i = 0; i < vals.size(); i++) {
            if (i <= (int) (vals.size() * 0.2)) {
                if (vals.get(i) <= oldVal) {
                    return median;
                }
            } else if (i >= (int) (vals.size() * 0.8)) {
                if (vals.get(i) >= oldVal) {
                    return median;
                }
            }
        }
        return oldVal;
    }

    //optymalne sortowanie - możesz wykorzystać je przy filtrach medianowych
    private static void quicksort(int tab[], int dol, int gora) {

        int i = dol, j = gora;
        int polowa = tab[dol + (gora - dol) / 2];

        while (i <= j) {
            while (tab[i] < polowa) {
                i++;
            }
            while (tab[j] > polowa) {
                j--;
            }
            if (i <= j) {
                int tmp = tab[i];
                tab[i] = tab[j];
                tab[j] = tmp;
                i++;
                j--;
            }
        }
        if (dol < j)
            quicksort(tab, dol, j);
        if (i < gora)
            quicksort(tab, i, gora);
    }

}
