import cv2
import numpy as np
from matplotlib import pyplot as plt

if __name__ == "__main__":
    img1 = cv2.imread('org/first.jpg')
    img2 = cv2.imread('org/second.jpg')
    img1gray = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    img2gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    ret, img1bin = cv2.threshold(img1gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    ret, img2bin = cv2.threshold(img2gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    kernel = np.ones((5,5),np.uint8)

    erosion1otsu = cv2.erode(img1bin,kernel,iterations = 1)
    erosion2otsu = cv2.erode(img2bin,kernel,iterations = 1)

    dilatation1otsu = cv2.dilate(img1bin,kernel,iterations = 1)
    dilatation2otsu = cv2.dilate(img2bin,kernel,iterations = 1)

    cv2.imwrite('erosion1otsu.jpg', erosion1otsu)
    cv2.imwrite('erosion2otsu.jpg', erosion2otsu)
    cv2.imwrite('dilatation1otsu.jpg', dilatation1otsu)
    cv2.imwrite('dilatation2otsu.jpg', dilatation2otsu)

    erosion1gray = cv2.erode(img1gray,kernel,iterations = 1)
    erosion2gray = cv2.erode(img2gray,kernel,iterations = 1)

    dilatation1gray = cv2.dilate(img1gray,kernel,iterations = 1)
    dilatation2gray = cv2.dilate(img2gray,kernel,iterations = 1)

    cv2.imwrite('erosion1gray.jpg', erosion1gray)
    cv2.imwrite('erosion2gray.jpg', erosion2gray)
    cv2.imwrite('dilatation1gray.jpg', dilatation1gray)
    cv2.imwrite('dilatation2gray.jpg', dilatation2gray)

    opening1bin = cv2.morphologyEx(img1bin, cv2.MORPH_OPEN, kernel)
    opening2bin = cv2.morphologyEx(img1bin, cv2.MORPH_OPEN, kernel)
    closing1bin = cv2.morphologyEx(img1bin, cv2.MORPH_CLOSE, kernel)
    closing2bin = cv2.morphologyEx(img1bin, cv2.MORPH_CLOSE, kernel)

    cv2.imwrite('opening1bin.jpg', opening1bin)
    cv2.imwrite('opening2bin.jpg', opening2bin)
    cv2.imwrite('closing1bin.jpg', closing1bin)
    cv2.imwrite('closing2bin.jpg', closing2bin)

    opening1gray = cv2.morphologyEx(img1gray, cv2.MORPH_OPEN, kernel)
    opening2gray = cv2.morphologyEx(img1gray, cv2.MORPH_OPEN, kernel)
    closing1gray = cv2.morphologyEx(img1gray, cv2.MORPH_CLOSE, kernel)
    closing2gray = cv2.morphologyEx(img1gray, cv2.MORPH_CLOSE, kernel)

    cv2.imwrite('opening1gray.jpg', opening1gray)
    cv2.imwrite('opening2gray.jpg', opening2gray)
    cv2.imwrite('closing1gray.jpg', closing1gray)
    cv2.imwrite('closing2gray.jpg', closing2gray)

    tophat1bin = cv2.morphologyEx(img1bin, cv2.MORPH_TOPHAT, kernel)
    tophat2bin = cv2.morphologyEx(img1bin, cv2.MORPH_TOPHAT, kernel)
    blackhat1bin = cv2.morphologyEx(img1bin, cv2.MORPH_BLACKHAT, kernel)
    blackhat2bin = cv2.morphologyEx(img1bin, cv2.MORPH_BLACKHAT, kernel)

    cv2.imwrite('tophat1bin.jpg', tophat1bin)
    cv2.imwrite('tophat2bin.jpg', tophat2bin)
    cv2.imwrite('blackhat1bin.jpg', blackhat1bin)
    cv2.imwrite('blackhat2bin.jpg', blackhat2bin)

    tophat1gray = cv2.morphologyEx(img1gray, cv2.MORPH_TOPHAT, kernel)
    tophat2gray = cv2.morphologyEx(img1gray, cv2.MORPH_TOPHAT, kernel)
    blackhat1gray = cv2.morphologyEx(img1gray, cv2.MORPH_BLACKHAT, kernel)
    blackhat2gray = cv2.morphologyEx(img1gray, cv2.MORPH_BLACKHAT, kernel)

    cv2.imwrite('tophat1gray.jpg', tophat1gray)
    cv2.imwrite('tophat2gray.jpg', tophat2gray)
    cv2.imwrite('blackhat1gray.jpg', blackhat1gray)
    cv2.imwrite('blackhat2gray.jpg', blackhat2gray)

    fourier1 = np.fft.fft2(img1)
    fshift1 = np.fft.fftshift(fourier1)
    magnitude_spectrum1 = 20*np.log(np.abs(fshift1))
    # cv2.imwrite('a.jpg', magnitude_spectrum1) # TODO

    img1template = cv2.imread('org/first-template.png',0)
    w, h = img1template.shape[::-1]
    res = cv2.matchTemplate(img1gray,img1template,cv2.TM_CCOEFF_NORMED)
    threshold = 0.8
    loc = np.where(res >= threshold)
    for pt in zip(*loc[::-1]):
        cv2.rectangle(img1, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)
    cv2.imwrite('templated1.png',img1)

    img2template = cv2.imread('org/second-template.png',0)
    w, h = img2template.shape[::-1]
    res = cv2.matchTemplate(img2gray,img2template,cv2.TM_CCOEFF_NORMED)
    threshold = 0.8
    loc = np.where(res >= threshold)
    for pt in zip(*loc[::-1]):
        cv2.rectangle(img2, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)
    cv2.imwrite('templated2.png',img2)

    img1blur = cv2.medianBlur(img1gray,5)
    cimg1 = cv2.cvtColor(img1blur,cv2.COLOR_GRAY2BGR)
    circles1 = cv2.HoughCircles(img1blur,cv2.HOUGH_GRADIENT,1,20,
                                param1=100,param2=50,minRadius=15,maxRadius=50)
    circles1 = np.uint16(np.around(circles1))
    for i in circles1[0,:]:
        # draw the outer circle
        cv2.circle(cimg1,(i[0],i[1]),i[2],(0,255,0),2)
        # draw the center of the circle
        cv2.circle(cimg1,(i[0],i[1]),2,(0,0,255),3)

    cv2.imwrite('circles1.jpg', cimg1)

    img2blur = cv2.medianBlur(img2gray,5)
    cimg2 = cv2.cvtColor(img2blur,cv2.COLOR_GRAY2BGR)
    circles1 = cv2.HoughCircles(img2blur,cv2.HOUGH_GRADIENT,1,20,
                                param1=100,param2=50,minRadius=15,maxRadius=50)
    circles1 = np.uint16(np.around(circles1))
    for i in circles1[0,:]:
        # draw the outer circle
        cv2.circle(cimg2,(i[0],i[1]),i[2],(0,255,0),2)
        # draw the center of the circle
        cv2.circle(cimg2,(i[0],i[1]),2,(0,0,255),3)

    cv2.imwrite('circles2.jpg', cimg2)

    img1 = cv2.imread('org/first.jpg')
    img2 = cv2.imread('org/second.jpg')

    ret, thresh = cv2.threshold(img1gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    kernel = np.ones((3,3),np.uint8) # noise removal
    opening = cv2.morphologyEx(thresh,cv2.MORPH_OPEN,kernel, iterations = 2)
    sure_bg = cv2.dilate(opening,kernel,iterations=3) # sure background area
    # Finding sure foreground area
    dist_transform = cv2.distanceTransform(opening,cv2.DIST_L2,5)
    ret, sure_fg = cv2.threshold(dist_transform,0.7*dist_transform.max(),255,0)
    sure_fg = np.uint8(sure_fg) # Finding unknown region
    unknown = cv2.subtract(sure_bg,sure_fg)
    ret, markers = cv2.connectedComponents(sure_fg) # marker labelling
    # Add one to all labels so that sure background is not 0, but 1
    markers = markers+1
    markers[unknown==255] = 0 # now, mark the region of unknown with zero
    markers = cv2.watershed(img1,markers)
    img1[markers == -1] = [255,0,0]
    cv2.imwrite('watershed1.jpg',img1)

    ret, thresh = cv2.threshold(img2gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    kernel = np.ones((3,3),np.uint8) # noise removal
    opening = cv2.morphologyEx(thresh,cv2.MORPH_OPEN,kernel, iterations = 2)
    sure_bg = cv2.dilate(opening,kernel,iterations=3) # sure background area
    # Finding sure foreground area
    dist_transform = cv2.distanceTransform(opening,cv2.DIST_L2,5)
    ret, sure_fg = cv2.threshold(dist_transform,0.7*dist_transform.max(),255,0)
    sure_fg = np.uint8(sure_fg) # Finding unknown region
    unknown = cv2.subtract(sure_bg,sure_fg)
    ret, markers = cv2.connectedComponents(sure_fg) # marker labelling
    # Add one to all labels so that sure background is not 0, but 1
    markers = markers+1
    markers[unknown==255] = 0 # now, mark the region of unknown with zero
    markers = cv2.watershed(img2,markers)
    img2[markers == -1] = [255,0,0]
    cv2.imwrite('watershed2.jpg',img2)

    img1 = cv2.imread('org/first.jpg')
    img2 = cv2.imread('org/second.jpg')

    rows,cols = img1gray.shape
    M = cv2.getRotationMatrix2D((cols/2,rows/2),45,1)
    dst = cv2.warpAffine(img1gray,M,(cols,rows))
    cv2.imwrite("rotated1.jpg", dst)

    rows,cols = img2gray.shape
    M = cv2.getRotationMatrix2D((cols/2,rows/2),45,1)
    dst = cv2.warpAffine(img2gray,M,(cols,rows))
    cv2.imwrite("rotated2.jpg", dst)

    gray = cv2.cvtColor(img1,cv2.COLOR_BGR2GRAY)
    corners = cv2.goodFeaturesToTrack(gray,25,0.01,10)
    corners = np.int0(corners)
    for i in corners:
        x,y = i.ravel()
        cv2.circle(img1,(x,y),3,255,-1)
    cv2.imwrite('descriptors1.jpg',img1)

    gray = cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)
    corners = cv2.goodFeaturesToTrack(gray,25,0.01,10)
    corners = np.int0(corners)
    for i in corners:
        x,y = i.ravel()
        cv2.circle(img2,(x,y),3,255,-1)
    cv2.imwrite('descriptors2.jpg',img2)